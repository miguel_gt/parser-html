# README #

### PARSER HTML - PARALLEL ###
Este proyecto consiste en la ejecución de algunos scripts programados de forma paralela en los lenguajes de programación  php y bash. El objetivo de estos scripts es efectuar un parseo (análisis) sobre un conjunto de archivos html con una estructura ya definida y organizados en un conjunto de carpetas. Los datos obtenidos durante el parseo son enviados a una base de datos de MySQL. 

El proyecto fue implementado como fase inicial de un sistema de consultas donde se requería tener una base de datos ya poblada con miles de registros para la implementación de pruebas futuras del sistema.