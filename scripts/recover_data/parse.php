
<?php
	include_once('simple_html_dom.php');
	
	/*
		Este código se utiliza para parsear los html de las carpetas T3 y T4 
		El resultado del parseo es enviando a la base de datos denominada seek de mysql,
		almacenando los datos de las empresas en la tabla Emp_Aux_2. Los datos que se obtienen del
		parseo son:
			
			1.- Nombre de la Empresa
			2.- Correo
			3.- Dirección
			4.- Localidad
			5.- Region
			6.- Telefono

		El logo tambien sera extraido de estos archivos, pero por el momento no se ha implementado
	*/
	
	function analizarHTML($ruta)
	{ 
		require "codigos_html.php";

		$conection = mysql_connect("localhost", "root", "root");
 		mysql_select_db("seek", $conection);
 		mysql_query("SET NAMES 'utf8'");
 		
 		$count = 0;

		if (is_dir($ruta)) 
	   		if ($dh = opendir($ruta)) 
	   		{
				 while (($file = readdir($dh)) !== false)//Se lee cada archivo html
			       	if($file != "." && $file != ".." && $file != "index.php")//Se verifica que el archivo por analizar sea html
		           	{
						$datos = Array();
		           		$html = file_get_html($ruta.$file);

						$datos[0] = $file;

						if($html->find('h1[class=organization-name]') == null)
							$datos[1] = "";
						else	
							$datos[1] = $html->find('h1[class=organization-name]')[0]->plaintext; //Nombre de la empresa
						
						if($html->find('a[title=correo]') == null)
							$datos[2] = "";
						else
							$datos[2] = $html->find('a[title=correo]')[0]->href; //Correo

						if($html->find('span[class=street-address]') == null)
							$datos[3] = "";
						else	
							$datos[3] = $html->find('span[class=street-address]')[0]->plaintext;//Direccion
						
						if($html->find('span[class=locality]') == null)
							$datos[4] = "";
						else	
							$datos[4] = $html->find('span[class=locality]')[0]->plaintext;//Localidad
						
						if($html->find('span[class=region]') == null)
							$datos[5] = "";
						else	
							$datos[5] = $html->find('span[class=region]')[0]->plaintext;//Region
						
						if($html->find('span[class=tel]') == null)
							$datos[6] = "";
						else	
							$datos[6] = $html->find('span[class=tel]')[0]->plaintext;//Telefono

						if($html->find('div[class=logo] img') == null)
							$datos[7] = "";
						else	
							$datos[7] = end(split('/',$html->find('div[class=logo] img')[0]->src));

						cambiarCodigosHTML($datos,$codigosHTML);
						agregarEmpresa($datos,$conection);
					}
				closedir($dh); 
	      	}
	}

	function cambiarCodigosHTML(&$datos,$codigosHTML)
	{
		for($i=1; $i<7; $i++)
			if($i!=6)
			{
				$cad = $datos[$i];
				if(strcmp($cad, "")!=0)
				{
					$aux = Array();
					$tamCad = strlen($cad);

					for($j=0; $j<$tamCad; $j++)
						if($cad[$j]=='&')
						{
							$array = Array();
							
							for($w=$j; $w<$tamCad && $cad[$w] != ';'; $w++,$j++)
								array_push($array, $cad[$w]);
							
							$codigo = implode("",$array);
							//echo " ".$codigo.":".$codigosHTML[$codigo]." ";
							array_push($aux, $codigosHTML[$codigo]);
						}else
							array_push($aux, $cad[$j]);
					
					$datos[$i] = implode("",$aux);
				}
			}
	}
						
	function agregarEmpresa($datos,$conection)
	{
		$query = "INSERT INTO Emp_Aux_2(archivo,nombre,correo,direccion,localidad,region,telefono,logo) VALUES('".
								$datos[0]."','".
								$datos[1]."','".
								$datos[2]."','".
								$datos[3]."','".
								substr($datos[4], 0,strlen($datos[4])-3)."','".
								substr($datos[5], 0,strlen($datos[5])-4)."','".
								substr($datos[6],5)."','".
								$datos[7]."')";
		
		mysql_query($query,$conection);
	}

	$path="/home/miguel/data-base_de/auxiliar_".$argv[1]."/";
	analizarHTML($path);
?>