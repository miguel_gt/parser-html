
<?php
	include_once('simple_html_dom.php');
	
	/*
		Este código se utiliza para parsear los html de las carpetas test1 y test2 
		El resultado del parseo es enviando a la base de datos llamada seek de mysql,
		almacenando los datos de las empresas en la tabla Emp_Aux_1. Los datos que se obtienen del
		parseo son:
			
			1.- Nombre de la Empresa
			2.- Dirección
			3.- Codigo postal
			4.- Localidad
			5.- Telefono
			6.- Categoria
			7.- Logo
	*/

	/*
	Descripcion:
		Esta función se encarga de parsear los archivos html para recuperar los datos de las empresas
	Parametros:
		$ruta -> Ruta de la carpeta que contiene los archivos html
	*/

	function analizarHTML($ruta)
	{
		require "codigos_html.php";
		
		$conection = mysql_connect("localhost", "root", "root");

 		mysql_select_db("seek", $conection);
 		mysql_query("SET NAMES 'utf8'");
 		
 		if (is_dir($ruta)) 
	   		if ($dh = opendir($ruta)) 
	   		{
				while (($file = readdir($dh)) !== false)//Se recuperan los archivos html
			       	if($file != "." && $file != ".." && $file != "index.php")
		           	{
						$html = file_get_html($ruta.$file);//Se obtiene el codigo html del archivo
						
						if($html->find('div[class=vcard]') != null)
							foreach($html->find('div[class=vcard]') as $item)
							{
								$datos = Array();
			           			
			           			$datos[0] = $file;

			           			if($item->find('div[class=organization-name] a span') == null)
			           				$datos[1] = "";
			           			else
			           				$datos[1] = $item->find('div[class=organization-name] a span')[0]->plaintext; //Nombre de la empresa
								
								if($item->find('div[class=address] span[class=street-address]') == null)
									$datos[2] = "";
								else	
									$datos[2] = $item->find('div[class=address] span[class=street-address]')[0]->plaintext;//Direccion
								
								if($item->find('div[class=address] span[class=postal-code]') == null)
									$datos[3] = "";
								else
									$datos[3] = $item->find('div[class=address] span[class=postal-code]')[0]->plaintext;//Codigo postal
								
								if($item->find('div[class=address] span[class=locality]') == null)
									$datos[4] = "";
								else
									$datos[4] = $item->find('div[class=address] span[class=locality]')[0]->plaintext;//localidad
								
								if( $item->find('div[class=phone] span[class=tel]') == null)
									$datos[5] = "";
								else
									$datos[5] = $item->find('div[class=phone] span[class=tel]')[0]->plaintext;//telefono

								if($item->find('div[class=categoria_link] span[class=catego] a') == null)
									$datos[6] = "";
								else
									$datos[6] = $item->find('div[class=categoria_link] span[class=catego] a')[0]->plaintext;//Categoria
							
								if($item->find('div[class=logo] a img') == null)
									$datos[7] = "";
								else
									$datos[7] = end(split('/',$item->find('div[class=logo] a img')[0]->src));
								
								cambiarCodigosHTML($datos,$codigosHTML);
	                           	agregarEmpresa($datos,$conection);
	                        }
					}
				
				closedir($dh); 
	      	}
	}
	
	function cambiarCodigosHTML(&$datos,$codigosHTML)
	{
		for($i=1; $i<7; $i++)
			if($i!=3 && $i!=5 && $i!=7)
			{
				$cad = $datos[$i];
			
				if(strcmp($cad, "")!=0)
				{
					$aux = Array();
					$tamCad = strlen($cad);

					for($j=0; $j<$tamCad; $j++)
						if($cad[$j]=='&')
						{
							$array = Array();
							
							for($w=$j; $w<$tamCad && $cad[$w] != ';'; $w++,$j++)
								array_push($array, $cad[$w]);
							
							$codigo = implode("",$array);
							//echo " ".$codigo.":".$codigosHTML[$codigo]." ";
							array_push($aux, $codigosHTML[$codigo]);
						}else
							array_push($aux, $cad[$j]);
					
					$datos[$i] = implode("",$aux);
				}
			}
	}

	function agregarEmpresa($datos,$conection)
	{
		$cp = substr($datos[3],4);

		$query = "INSERT INTO Emp_Aux_1(archivo,nombre,direccion,cp,localidad,telefono,categoria,logo) VALUES('".
								$datos[0]."','".
								$datos[1]."','".
								$datos[2]."','".
								substr($cp,0,strlen($cp-5))."','".
								$datos[4]."','".
								substr($datos[5],5) ."','".
								$datos[6]."','".
								$datos[7]."')";
	
		mysql_query($query,$conection);
	}

	$path="/home/miguel/data-base_de/auxiliar_".$argv[1]."/";
	analizarHTML($path);
?>