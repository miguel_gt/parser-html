
select count(*) from Emp_Aux_2;

select count(*) from Emp_Aux_2
where correo != '';

select id,TRIM(BOTH ' ' FROM nombre) as empresa,TRIM(LEADING 'mailto:' FROM correo) as email,direccion,localidad,region,telefono,logo
from Emp_Aux_2
where correo != '' and region like '%OAXACA%'
order by empresa;

alter table Emp_Aux_1
add column email varchar(100) null;

/*
	Consulta para agregar Empresas que no estan en la tabla Emp_Aux_1 que cuentan con email.
*/
select *
from Emp_Aux_2
where correo != ''
and TRIM(BOTH ' ' FROM nombre) not in (
											select distinct(TRIM(BOTH ' ' FROM E.nombre))
											from Emp_Aux_1 E
										)
order by nombre asc;

INSERT INTO Emp_Aux_2(archivo,nombre,telefono,logo,locality,address,region,email) 
	   VALUES( (select E2.archivo,TRIM(BOTH ' ' FROM E2.nombre),E2.telefono,E2.logo,E2.localidad,E2.direccion,E2.region,TRIM(LEADING 'mailto:' FROM E2.correo)
				from Emp_Aux_2 E2
				where E2.correo != ''
				and TRIM(BOTH ' ' FROM E2.nombre) in (select distinct(TRIM(BOTH ' ' FROM E.nombre))
									 from Emp_Aux_1 E
								     )
));

select E2.archivo,TRIM(BOTH ' ' FROM E2.nombre) as empresa,E2.telefono,E2.logo,E2.localidad,E2.direccion,E2.region,TRIM(LEADING 'mailto:' FROM E2.correo)
				from Emp_Aux_2 E2
				where E2.correo != ''
				and TRIM(BOTH ' ' FROM E2.nombre) in (select distinct(TRIM(BOTH ' ' FROM E.nombre))
									 from Emp_Aux_1 E
								     )
order by empresa asc;



