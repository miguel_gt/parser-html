use seek;

#Obtener el numero de registros no repetidos y sin importar el logo
select E1.*, E2.*
from 
	(	select TRIM(BOTH ' ' FROM nombre) as empresa,direccion,cp,localidad,telefono,categoria,logo
		from Emp_Aux_1 ) as E1
	LEFT JOIN 
	(	select TRIM(BOTH ' ' FROM nombre) as empresa,TRIM(LEADING 'mailto:' FROM correo) as email,direccion,localidad,region,telefono,logo
		from Emp_Aux_2 ) as E2 
	on (E1.empresa = E2.empresa);

select TRIM(BOTH ' ' FROM nombre) as empresa,direccion,cp,localidad,telefono,categoria,logo
		from Emp_Aux_1  order by empresa

select TRIM(BOTH ' ' FROM nombre) as empresa,TRIM(LEADING 'mailto:' FROM correo) as email,direccion,localidad,region,telefono,logo
		from Emp_Aux_2
order by empresa asc
