
#Eliminacion de registros repetidos
delete from Emp_Aux_2 
where id not in ( select X.id 
					from ( 
					       select id,TRIM(BOTH ' ' FROM nombre) as empresa, direccion,localidad,region,telefono,TRIM(LEADING 'mailto:' FROM correo) as email,logo
					       from Emp_Aux_2
						   group by nombre, direccion,localidad, region,telefono,correo,logo
					) X
			 );

#Total de archivos eliminados -> 1.294.783

#Numero de registros despues de la eliminacio -> 190080
select count(*) from Emp_Aux_2 where correo != ''


