
#Total de registros en la tabla -> 1.982.195
select count(*) from Emp_Aux_1;

#Numero de registros sin repetir, pero pueden o no pueden tener numero telefonico
#Aqui no se descartan los registros que no tienen numero, ya que hay posibilidad de que el
#registro exista en la tabla Emp_Aux_2 y cuente con numero y correo.
select id,TRIM(BOTH ' ' FROM nombre) as empresa,direccion,localidad,cp,telefono,categoria,logo
from Emp_Aux_1
group by nombre, direccion,localidad,cp,telefono,categoria,logo;
#Total -> 1.158.803

#Eliminacion de registros repetidos
delete from Emp_Aux_1 
where id not in ( select X.id 
					from ( 
					       select id,TRIM(BOTH ' ' FROM nombre) as empresa,direccion,localidad,cp,telefono,categoria,logo
					       from Emp_Aux_1
						   group by nombre, direccion,localidad,cp,telefono,categoria,logo
					) X
			 );
#Total de archivos eliminados -> 823.392

#Numero de registros despues de la eliminacion -> 1.158.803
select count(*) from Emp_Aux_1;
#El resultado de este count debe ser igual al resultado de la primera consulta de este archivo, ya que
#solo quedan los registros que no se repiten.
#Con estos registros ya no va a ser necesario hacer la agrupación para efectuar el left join entre las dos tablas.
#Esto nos puede ahorrar tiempo al momento de hacer el join, ya que nos evitamos la funcion del group by sobre los registros
select * from Emp_Aux_1 order by nombre;

