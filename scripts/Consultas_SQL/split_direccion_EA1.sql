
#Agregando los atributos de la nueva direccion
alter table Emp_Aux_1 add column locality varchar(200) null;
alter table Emp_Aux_1 add column address varchar(200) null;
alter table Emp_Aux_1 add column region varchar(100) null;

#Procedimiento almacenado para aplicar la funcionalidad del split en el campo direccion
DELIMITER $
DROP PROCEDURE IF EXISTS `seek`.`split` $
CREATE procedure split()
BEGIN
	DECLARE address VARCHAR(200);
	DECLARE id_Emp INT;
	DECLARE dir VARCHAR(200);
	DECLARE dirAux VARCHAR(200);
	DECLARE loc VARCHAR(200);
	DECLARE reg VARCHAR(200);
	DECLARE vb_termina BOOL;

	#Se recupera una lista de productos en forma aleatoria
	DECLARE direcciones CURSOR FOR SELECT id,direccion FROM Emp_Aux_1; #limit 100;
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET vb_termina = TRUE;
	
	OPEN direcciones;
	Recorre_Cursor: LOOP
		FETCH direcciones INTO id_Emp,address;
			IF vb_termina THEN
				LEAVE Recorre_Cursor;
			END IF;

			#Se quitan los espacios al inicio y al final de la direccion.
			#Y se quita la coma que aparece al final 
			SET dirAux = TRIM(TRAILING ',' FROM TRIM(BOTH ' ' FROM address) );
			
			#Se hace el split para dirAux
			SET dir = TRIM(BOTH ' ' FROM SUBSTRING_INDEX(dirAux , ',', 1 ));
			SET loc = TRIM(BOTH ' ' FROM SUBSTRING_INDEX(SUBSTRING_INDEX( dirAux , ',', 2 ),',',-1));
			SET reg = TRIM(BOTH ' ' FROM SUBSTRING_INDEX(SUBSTRING_INDEX( dirAux , ',', -2 ),',',-1));

			#Actualizar los datos de las nuevas direcciones
			UPDATE Emp_Aux_1 SET address = dir, locality = loc, region = reg where id = id_Emp;
	
	END LOOP;
	CLOSE direcciones;
END $
DELIMITER ;

call split();
select direccion, address, locality, region from Emp_Aux_1 limit 500;

