/*
Esta consulta es para eliminar los registros que no tienen correo y telefono.
Si no cuentan con esta información no es necesario tenerlos en la tabla.
*/

#Numero de registros antes de la eliminacion
select count(*) from Emp_Aux_2;
#Total->190080

#Eliminar registros que no tienen correo y numero telefónico
delete from Emp_Aux_2
where id in (	select *
				from ( 
						select id
						from Emp_Aux_2
						where correo = '' and telefono = ''
					  ) X
			 );
#Total de archivos eliminados -> 2226

#Numero de registros despues de la eliminacion
select count(*) from Emp_Aux_2;
#Total->187854

select * from Emp_Aux_2 order by nombre;

select TRIM(BOTH ' ' FROM nombre) empresa,  correo, count(*) as total_correos
from Emp_Aux_2
where correo != ''
group by empresa , correo
having count(*) = 1

select distinct(nombre) from Emp_Aux_2 where correo != ''