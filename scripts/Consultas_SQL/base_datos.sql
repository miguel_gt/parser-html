DROP table Companies;

CREATE TABLE `Companies` (
  `company_id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(50) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `region` varchar(100) DEFAULT NULL,
  `locality` varchar(100) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `cp` varchar(5) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `category` varchar(100) DEFAULT NULL,
  `logo` varchar(100) DEFAULT NULL
);

#Poblando tabla
INSERT INTO Companies
SELECT TRIM(BOTH ' ' FROM nombre) empresa,address,region,locality,localidad,cp,telefono,'',categoria,logo 
FROM Emp_Aux_1;

select * from Emp_Aux_1 limit 20;

