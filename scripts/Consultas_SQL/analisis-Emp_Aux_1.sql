use seek;

#Total de registros obtenidos -> 1982195
select count(*) from Emp_Aux_1;

#Registros sin nombre de empresa
select count(*)
from Emp_Aux_1
where nombre != '';

#Registros que no tienen numero telefónico
select nombre, id,telefono,direccion
from Emp_Aux_1
where telefono = ''
order by nombre

select count(*)
from Emp_Aux_1
where direccion = ''

#Registros repetidos
select nombre, direccion, cp, localidad, telefono,categoria,logo
from Emp_Aux_1
where telefono != '' and localidad = 'SLP' 
group by nombre, direccion, cp, localidad, telefono,categoria,logo


select distinct(nombre)
from Emp_Aux_1
where logo = '' and telefono = ''
order by nombre asc;

#Total de empresas, con numero de contacto y logo -> 659360
select nombre, direccion, cp, localidad, telefono,categoria, logo 
from Emp_Aux_1
where telefono != '' and nombre = 'GLOBAL PC NET'
group by nombre, direccion, cp, localidad, telefono,categoria,logo
order by nombre, direccion asc;



