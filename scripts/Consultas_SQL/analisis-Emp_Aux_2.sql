
#Total de registros despues del analisis
select count(*) from Emp_Aux_2;

#Registros sin nombre de empresa
select count(*) from Emp_Aux_2 where nombre != '';

#Total de registros que tienen nombre de la empresa pero no tienen telefono
select count(*) 
from Emp_Aux_2 
where nombre != '' and telefono = ''; 

#Total de registros que tienen nombre de la empresa pero no tienen correo
select count(*) 
from Emp_Aux_2 
where nombre != '' and correo = ''; 

#Total de registros que tienen nombre de empresa, pero no tienen telefono y correo
select nombre, telefono, correo
from Emp_Aux_2 
where nombre != '' and (telefono = '' and correo = '');

#Total de registros que tienen nombre de empresa, pero pueden tener telefono o correo -> 1.480.173

select count(*) 
from Emp_Aux_2 
where nombre != '' and (telefono != '' or correo != '');

#Obtener el numero de registros no repetidos y sin logo repiten
select nombre, direccion, correo, localidad,region,telefono,logo
from Emp_Aux_2
where nombre != '' and (telefono != '' or correo != '')
group by nombre, direccion, correo, localidad, region,telefono,logo
having logo = ''
order by nombre, direccion asc;

#Obtener el numero de registros no repetidos y sin importar el logo
select TRIM(BOTH ' ' FROM nombre) as empresa, direccion,localidad,region,telefono,TRIM(LEADING 'mailto:' FROM correo) as email,logo,id
from Emp_Aux_2
where nombre = 'ABRASIVOS INDUSTRIALES DE LA F ' and (telefono != '' or correo != '')
group by nombre, direccion,localidad, region,telefono,correo,logo
order by nombre, direccion asc;

#Total de empresas registradas en la tabla
select distinct(nombre)
from Emp_Aux_2
where nombre != ''
order by nombre asc;

select nombre from Emp_Aux_2 where id = 1492858
select id, nombre from Emp_Aux_2
order by nombre asc

select count(*)
from Emp_Aux_2
where nombre != ''

select LENGTH((select nombre from Emp_Aux_2 where id = 1492858));

SELECT TRIM(BOTH ' ' FROM '   bar  z ');



